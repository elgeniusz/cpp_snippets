size_t getBitCount(uint32_t var)
{
    size_t count = 0U;
    while(var > 0U)
    {
        ++count;
        var &= static_cast<uint32_t>(var - 1U);
    }

    return count;
}