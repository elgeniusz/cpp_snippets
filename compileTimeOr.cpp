#include <iostream>

//'Z' = 0x5A = 0101 1010
constexpr char testMasks[] = { 0x40, 0x10, 0x08, 0x02 };
const size_t testMasksCount = (sizeof(testMasks)) / (sizeof(testMasks[0]));

template<const size_t masksCount, const char* masks>
struct CompileTimeOr
{
    static char exe()
    {
        return masks[masksCount - 1] | CompileTimeOr<masksCount - 1, masks>::exe();
    }
};

template<const char* masks>
struct CompileTimeOr<1, masks>
{
    static char exe()
    {
        return masks[0];
    }
};

using namespace std;

int main()
{
    const char result = CompileTimeOr<testMasksCount, testMasks>::exe();
    cout << result;

    return 0;
}