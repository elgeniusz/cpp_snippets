bool isPowerOfTwo(uint32_t var)
{
    return (var & (var - 1)) == 0U);
}