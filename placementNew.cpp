struct Foo
{
    uint32_t wow[20];
}

uint8_t pool[1024];

Foo& createFoo()
{
    return new(pool) Foo;
}